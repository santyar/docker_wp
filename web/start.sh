#!/bin/bash

nginx -g "daemon off;"
php-fpm -D
chown nginx. /var/run/php-fpm.sock
